package com.darrentmacdonald.minecraft;

import org.apache.logging.log4j.LogManager;

import jssc.SerialPortException;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.Packet;
import net.minecraft.network.packet.s2c.play.EntitySpawnS2CPacket;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import java.util.Timer;
import java.util.TimerTask;



public class RocketEntity extends Entity {
	
	private static SerialITSReader r = null;
	
	private static RocketEntity theRocket = null;
	private static Timer theTimer = null;
	
	private float myRoll;
	
	public RocketEntity(EntityType<? extends RocketEntity> type, World world) {
		super(type, world);
		setInvulnerable(false);
		this.initDataTracker();
		System.out.println("Rocket constructed");
		if (!world.isClient) {
			theRocket = this;
			if (theTimer != null) {
				theTimer.cancel();
			}
			theTimer = new Timer();
			theTimer.schedule(new TimerTask() {
	
				@Override
				public void run() {
					// TODO Auto-generated method stub
					try {
						if (r == null) {
							r = new SerialITSReader();
							LogManager.getLogger().debug("Serial created!!!!!");
						} else {
							float[] rotation_data;
							rotation_data = r.getLatestAngle();
							Vec3d my = theRocket.getPos();
							
							theRocket.updatePositionAndAngles(my.x, my.y, my.z, rotation_data[0], rotation_data[1]); // Yaw and pitch
							
							myRoll = rotation_data[2];
							
							//this.setRotation(rotation_data[0], rotation_data[1]);
							
							LogManager.getLogger().debug(String.format("update %f %f %f", rotation_data[0], rotation_data[1], rotation_data[2]));
							
						}
					} catch (SerialPortException e) {
						// TODO Auto-generated catch block
						LogManager.getLogger().warn(e.getExceptionType());
					}
			}}, 1000, 50);
		}
	}
	
	
	@Override
	protected void afterSpawn() {
		// TODO Auto-generated method stub
		System.out.println("After Spawned");
		super.afterSpawn();
	}

	@Override
	public Packet<?> createSpawnPacket() {

		return new EntitySpawnS2CPacket(this);
	}

	@Override
	public void readCustomDataFromTag(CompoundTag tag) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void writeCustomDataToTag(CompoundTag tag) {
	}

	@Override
	public boolean handleAttack(Entity x) {
		System.out.println("Rocket Damaged!");
		return true;
	}

	@Override
	public boolean damage(DamageSource source, float amount) {
		System.out.println("Rocket Damaged!");
		this.kill();
		return true;
		// return super.damage(source, amount);
	}

	@Override
	public ActionResult interact(PlayerEntity player, Hand hand) {
		System.out.println("Rocket Interacted");
		// TODO Auto-generated method stub
		return super.interact(player, hand);
	}


	@Override
	protected void initDataTracker() {
		// TODO Auto-generated method stub
		
	}
	
	// The system calls getHeadYaw and setHeadYaw to transit the information in the HeadYawPacket, limited to +90 -90
	@Override
	public float getHeadYaw() {
		// Get the roll packed into +90 to -90
		return myRoll/2.0F;
	}

	
	@Override
	public void setHeadYaw(float a) {
		//System.out.print("Set Head Yaw ");
		if (world.isClient()) {
			//System.out.print("On Client"); 
			//System.out.println(a);
			myRoll = a*2.0F;
		} else {
			//System.out.print("On Server");
			//System.out.println(a);
		}
			
	}

	
}
