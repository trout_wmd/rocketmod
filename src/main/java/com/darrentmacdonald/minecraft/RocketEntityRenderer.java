package com.darrentmacdonald.minecraft;

import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.EntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.Quaternion;

public class RocketEntityRenderer extends EntityRenderer<RocketEntity> {

	protected final RocketEntityModel rocketModel = new RocketEntityModel();
	
	protected RocketEntityRenderer(EntityRenderDispatcher dispatcher) {
		super(dispatcher);
		
	}
	
	@Override
	public void render(RocketEntity entity, float rotationYaw, float partialTicks, MatrixStack matrixStack, VertexConsumerProvider vertexConsumers, int packedLight) {
		VertexConsumer vertexConsumer = vertexConsumers.getBuffer(rocketModel.getLayer(getTexture(entity)));
		

		matrixStack.multiply(new Quaternion(Vector3f.NEGATIVE_Y, entity.getYaw(0), true));
		matrixStack.multiply(new Quaternion(Vector3f.POSITIVE_X, entity.getPitch(0), true));
		matrixStack.multiply(new Quaternion(Vector3f.NEGATIVE_Z, entity.getHeadYaw()*2.0F, true)); // Roll
        rocketModel.render(matrixStack, vertexConsumer, packedLight, OverlayTexture.DEFAULT_UV, 1.0F, 1.0F, 1.0F, 1.0F);


        super.render(entity, rotationYaw, partialTicks, matrixStack, vertexConsumers, packedLight);
    }



	@Override
	public Identifier getTexture(RocketEntity entity) {
		 
	    return new Identifier("rocketmod", "textures/entity/rocket/rocket.png");
	}
}
