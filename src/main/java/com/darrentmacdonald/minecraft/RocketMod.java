package com.darrentmacdonald.minecraft;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents.ServerStarted;

public class RocketMod implements ModInitializer, ServerStarted {
    private static final Logger log = LogManager.getLogger();

	
	public static final EntityType<RocketEntity> ROCKET_ENTITY_TYPE = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier("rocketmod", "rocket"),
            FabricEntityTypeBuilder.create(SpawnGroup.MISC, RocketEntity::new).dimensions(EntityDimensions.fixed(1f, 1f)).build()
    );
	
	
	@Override
	public void onInitialize() {
		System.out.println("RocketMod: Prepare to rock!");
		ServerLifecycleEvents.SERVER_STARTED.register(this);
	}
	
	@Override
	public void onServerStarted(MinecraftServer server) {
		log.debug("Server Started");
	}
}
