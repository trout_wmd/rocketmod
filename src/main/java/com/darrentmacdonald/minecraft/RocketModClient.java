package com.darrentmacdonald.minecraft;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry;
import net.fabricmc.api.EnvType;


@Environment(EnvType.CLIENT)
public class RocketModClient implements ClientModInitializer {


	@Override
	public void onInitializeClient() {

		EntityRendererRegistry.INSTANCE.register(RocketMod.ROCKET_ENTITY_TYPE, (dispatcher, context) -> {
            return new RocketEntityRenderer(dispatcher);
        });
		System.out.println("Rocket Client Initialized");
	}
}
