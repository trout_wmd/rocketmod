package com.darrentmacdonald.minecraft;

import java.util.Arrays;
import java.util.Enumeration;
import java.lang.Math;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jssc.*;

public class SerialITSReader {
	
	private static final Logger log = LogManager.getLogger();
	private SerialPort port;
	private StringBuffer buf;		
	private float[] lastYPR = new float[3];
	private float[] q = new float[4];
	
	
	public SerialITSReader() throws SerialPortException {

		// port = new SerialPort("COM4");
		//port = new SerialPort("/dev/cu.usbmodem143401");
		String[] s = SerialPortList.getPortNames();
		for (String ss : s) {
			port = new SerialPort(ss);
			if (port.openPort()) {
				System.out.println("Opened serial port " + ss);
				break;
			}
		}
		
		if (port == null || !port.isOpened()) 
			throw new SerialPortException("Could not open any of:".concat(String.join(", ", s)), "SerialITSReader()", SerialPortException.TYPE_PORT_NOT_OPENED);
		port.setParams(9600,  8, 1, 0);
		buf = new StringBuffer();			
	}

	
	public float[] getLatestAngle() throws SerialPortException {
		
		
		String d = port.readString();
		if (d != null) {
			log.debug(String.format("Just read %s", d));
			buf.append(d);
			int ii = buf.lastIndexOf("\n");
			int i = buf.lastIndexOf("QAT:", ii);
			if (i >= 0 && ii >= 0) { 
				String s = buf.substring(i+4, ii);
				try {
					String[] m = s.trim().split(" ");
					for (int j = 0; j < 4; j++) {
						q[j] = Float.parseFloat(m[j]);
					}
				} catch (NumberFormatException e) {
					System.out.println(e.getMessage());
				} catch (ArrayIndexOutOfBoundsException e) {
					System.out.println(e.getMessage());
				}
				
				buf = new StringBuffer(buf.substring(ii));
				
				lastYPR[2] = (float) Math.atan2(q[0]*q[1] + q[2]*q[3], 0.5f - q[1]*q[1] - q[2]*q[2]) * 57.29578f;
				
				double x = -2.0f * (q[1]*q[3] - q[0]*q[2]);
				if (x >= 1.0) {
					lastYPR[1] = 90.0f;
				} else if (x <= -1.0) {
					lastYPR[1] = -90.0f;
				} else {
					lastYPR[1] = (float) Math.asin(x) * 57.29578f;
				}
				
				lastYPR[0] = (float) Math.atan2(q[1]*q[2] + q[0]*q[3], 0.5f - q[2]*q[2] - q[3]*q[3]) * 57.29578f;

			}
		} else {
			//log.debug("No Serial read");
		}
		
		return lastYPR;
	} 
	
	public static void main(String[] argv) throws SerialPortException, InterruptedException {
		
		
		SerialITSReader r = new SerialITSReader();
		
		int ctr = 0;
		while (true) {
			float[] a = r.getLatestAngle();
			//System.out.println(String.format("%f %f %f", a[0], a[1], a[2]));
			
			Thread.sleep(5);
		}
		
	}
}


